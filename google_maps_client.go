package main

import (
	"googlemaps.github.io/maps"
	"log"
	"os"
)

var googleMapsClient *maps.Client
func init()  {
	c, err := maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}
	googleMapsClient = c
}

