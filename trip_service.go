package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"googlemaps.github.io/maps"
	"routes-trip-service/proto"
)

func AutoCompleteAddress(ctx context.Context, req *proto.AutoCompleteAddressRequest) (*proto.AutoCompleteAddressResponse, error) {
	result, err := googleMapsClient.PlaceAutocomplete(context.Background(), &maps.PlaceAutocompleteRequest{
		Input: req.Description,
		Components: map[maps.Component][]string{
			"country": {"ng"},
		},
	})

	if err != nil {
		fmt.Println(err)
		return nil, status.Errorf(codes.Internal,
			"Unable to get predictions")

	}

	var addresses []*proto.Address

	for _, prediction := range result.Predictions {
		addresses = append(addresses, &proto.Address{
			Description: prediction.Description,
			PlaceId:     prediction.PlaceID,
		})
	}

	return &proto.AutoCompleteAddressResponse{
		Addresses: addresses,
	}, nil

}
