package main

import (
	"context"
	"fmt"
	"gitlab.com/connectroutes/routes-go-lib/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"googlemaps.github.io/maps"
	"routes-trip-service/proto"
)

type PassengerService struct {
}

func (s PassengerService) SetDefaultAddress(ctx context.Context, request *proto.SetDefaultAddressRequest) (*proto.SetDefaultAddressResponse, error) {

	user, _ := models.GetUserFromContext(ctx)

	result, err := googleMapsClient.PlaceDetails(context.Background(), &maps.PlaceDetailsRequest{
		PlaceID: request.PlaceId,
		Fields: []maps.PlaceDetailsFieldMask{
			maps.PlaceDetailsFieldMaskGeometry,
		},
	})

	if err != nil {
		fmt.Print(err)
		return nil, status.Errorf(codes.Internal,
			"Unable to decode place id")
	}

	addressType := "home_address"

	if request.GetType() == proto.AddressType_work {
		addressType = "work_address"
	}

	id, _ := primitive.ObjectIDFromHex(user.UserId)
	err = models.UpdateUser(context.Background(), db, id, map[string]interface{}{
		addressType: map[string]interface{}{
			"description": request.Description,
			"place_id":    request.PlaceId,
			"location": map[string]interface{}{
				"type": "Point",
				"coordinates": []float64{
					result.Geometry.Location.Lng,
					result.Geometry.Location.Lat,
				},
			},
		},
	})

	if err != nil {
		fmt.Print(err)
		return nil, status.Errorf(codes.Internal,
			"Unable to update address")
	}

	return &proto.SetDefaultAddressResponse{
		Message: "ok",
	}, nil

}

func (s PassengerService) TripHistory(context.Context, *proto.TripHistoryRequest) (*proto.TripHistoryResponse, error) {

	//TODO implement
	return &proto.TripHistoryResponse{
		Trips: []*proto.Trip{},
		Page:  0,
		Size:  0,
		Total: 0,
	}, nil
}

func (s PassengerService) AutoCompleteAddress(ctx context.Context, req *proto.AutoCompleteAddressRequest) (*proto.AutoCompleteAddressResponse, error) {
	return AutoCompleteAddress(ctx, req)

}
