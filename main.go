package main

import (
	"fmt"
	_ "github.com/joho/godotenv/autoload"
	"google.golang.org/grpc"
	"log"
	"net"
	"routes-trip-service/proto"
)

func main() {
	lis, err := net.Listen("tcp", "0.0.0.0:50054")

	if err != nil {
		log.Fatalf("Could not listen on port: %v", err)
	}
	fmt.Println("Server Started")

	server := grpc.NewServer()

	proto.RegisterPassengerTripServiceServer(server, &PassengerService{})

	//TODO add interceptor to validate driver
	proto.RegisterDriverTripServiceServer(server, &DriverService{})

	if err := server.Serve(lis); err != nil {
		log.Fatal(err)
	}
}
