module routes-trip-service

go 1.12

require (
	github.com/golang/protobuf v1.4.1
	github.com/joho/godotenv v1.3.0
	github.com/sergi/go-diff v1.1.0 // indirect
	gitlab.com/connectroutes/routes-go-lib v0.0.0-20201019182009-e8bfc8ce794d
	go.mongodb.org/mongo-driver v1.3.0
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	google.golang.org/grpc v1.27.1
	google.golang.org/protobuf v1.25.0
	googlemaps.github.io/maps v0.0.0-20200130222743-aef6b08443c7
)
