// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.13.0
// source: trip.passenger.proto

package proto

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type AddressType int32

const (
	AddressType_home AddressType = 0
	AddressType_work AddressType = 1
)

// Enum value maps for AddressType.
var (
	AddressType_name = map[int32]string{
		0: "home",
		1: "work",
	}
	AddressType_value = map[string]int32{
		"home": 0,
		"work": 1,
	}
)

func (x AddressType) Enum() *AddressType {
	p := new(AddressType)
	*p = x
	return p
}

func (x AddressType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (AddressType) Descriptor() protoreflect.EnumDescriptor {
	return file_trip_passenger_proto_enumTypes[0].Descriptor()
}

func (AddressType) Type() protoreflect.EnumType {
	return &file_trip_passenger_proto_enumTypes[0]
}

func (x AddressType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use AddressType.Descriptor instead.
func (AddressType) EnumDescriptor() ([]byte, []int) {
	return file_trip_passenger_proto_rawDescGZIP(), []int{0}
}

type TripHistoryRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Page int32 `protobuf:"varint,1,opt,name=page,proto3" json:"page,omitempty"`
	Size int32 `protobuf:"varint,2,opt,name=size,proto3" json:"size,omitempty"`
}

func (x *TripHistoryRequest) Reset() {
	*x = TripHistoryRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_trip_passenger_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TripHistoryRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TripHistoryRequest) ProtoMessage() {}

func (x *TripHistoryRequest) ProtoReflect() protoreflect.Message {
	mi := &file_trip_passenger_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TripHistoryRequest.ProtoReflect.Descriptor instead.
func (*TripHistoryRequest) Descriptor() ([]byte, []int) {
	return file_trip_passenger_proto_rawDescGZIP(), []int{0}
}

func (x *TripHistoryRequest) GetPage() int32 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *TripHistoryRequest) GetSize() int32 {
	if x != nil {
		return x.Size
	}
	return 0
}

type TripHistoryResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Trips []*Trip `protobuf:"bytes,1,rep,name=trips,proto3" json:"trips,omitempty"`
	Page  int32   `protobuf:"varint,2,opt,name=page,proto3" json:"page,omitempty"`
	Size  int32   `protobuf:"varint,3,opt,name=size,proto3" json:"size,omitempty"`
	Total int32   `protobuf:"varint,4,opt,name=total,proto3" json:"total,omitempty"`
}

func (x *TripHistoryResponse) Reset() {
	*x = TripHistoryResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_trip_passenger_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TripHistoryResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TripHistoryResponse) ProtoMessage() {}

func (x *TripHistoryResponse) ProtoReflect() protoreflect.Message {
	mi := &file_trip_passenger_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TripHistoryResponse.ProtoReflect.Descriptor instead.
func (*TripHistoryResponse) Descriptor() ([]byte, []int) {
	return file_trip_passenger_proto_rawDescGZIP(), []int{1}
}

func (x *TripHistoryResponse) GetTrips() []*Trip {
	if x != nil {
		return x.Trips
	}
	return nil
}

func (x *TripHistoryResponse) GetPage() int32 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *TripHistoryResponse) GetSize() int32 {
	if x != nil {
		return x.Size
	}
	return 0
}

func (x *TripHistoryResponse) GetTotal() int32 {
	if x != nil {
		return x.Total
	}
	return 0
}

type SetDefaultAddressRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PlaceId     string      `protobuf:"bytes,1,opt,name=place_id,json=placeId,proto3" json:"place_id,omitempty"`
	Description string      `protobuf:"bytes,2,opt,name=description,proto3" json:"description,omitempty"`
	Type        AddressType `protobuf:"varint,3,opt,name=type,proto3,enum=com.connectroutes.trip.AddressType" json:"type,omitempty"`
}

func (x *SetDefaultAddressRequest) Reset() {
	*x = SetDefaultAddressRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_trip_passenger_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetDefaultAddressRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetDefaultAddressRequest) ProtoMessage() {}

func (x *SetDefaultAddressRequest) ProtoReflect() protoreflect.Message {
	mi := &file_trip_passenger_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetDefaultAddressRequest.ProtoReflect.Descriptor instead.
func (*SetDefaultAddressRequest) Descriptor() ([]byte, []int) {
	return file_trip_passenger_proto_rawDescGZIP(), []int{2}
}

func (x *SetDefaultAddressRequest) GetPlaceId() string {
	if x != nil {
		return x.PlaceId
	}
	return ""
}

func (x *SetDefaultAddressRequest) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *SetDefaultAddressRequest) GetType() AddressType {
	if x != nil {
		return x.Type
	}
	return AddressType_home
}

type SetDefaultAddressResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Message string `protobuf:"bytes,1,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *SetDefaultAddressResponse) Reset() {
	*x = SetDefaultAddressResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_trip_passenger_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetDefaultAddressResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetDefaultAddressResponse) ProtoMessage() {}

func (x *SetDefaultAddressResponse) ProtoReflect() protoreflect.Message {
	mi := &file_trip_passenger_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetDefaultAddressResponse.ProtoReflect.Descriptor instead.
func (*SetDefaultAddressResponse) Descriptor() ([]byte, []int) {
	return file_trip_passenger_proto_rawDescGZIP(), []int{3}
}

func (x *SetDefaultAddressResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

var File_trip_passenger_proto protoreflect.FileDescriptor

var file_trip_passenger_proto_rawDesc = []byte{
	0x0a, 0x14, 0x74, 0x72, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65, 0x72,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x16, 0x63, 0x6f, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x6e,
	0x65, 0x63, 0x74, 0x72, 0x6f, 0x75, 0x74, 0x65, 0x73, 0x2e, 0x74, 0x72, 0x69, 0x70, 0x1a, 0x0a,
	0x74, 0x72, 0x69, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x3c, 0x0a, 0x12, 0x54, 0x72,
	0x69, 0x70, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04,
	0x70, 0x61, 0x67, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x22, 0x87, 0x01, 0x0a, 0x13, 0x54, 0x72, 0x69,
	0x70, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x32, 0x0a, 0x05, 0x74, 0x72, 0x69, 0x70, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x72, 0x6f, 0x75,
	0x74, 0x65, 0x73, 0x2e, 0x74, 0x72, 0x69, 0x70, 0x2e, 0x54, 0x72, 0x69, 0x70, 0x52, 0x05, 0x74,
	0x72, 0x69, 0x70, 0x73, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x04, 0x70, 0x61, 0x67, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x73, 0x69, 0x7a, 0x65,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x12, 0x14, 0x0a, 0x05,
	0x74, 0x6f, 0x74, 0x61, 0x6c, 0x18, 0x04, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x74, 0x6f, 0x74,
	0x61, 0x6c, 0x22, 0x90, 0x01, 0x0a, 0x18, 0x53, 0x65, 0x74, 0x44, 0x65, 0x66, 0x61, 0x75, 0x6c,
	0x74, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x19, 0x0a, 0x08, 0x70, 0x6c, 0x61, 0x63, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x07, 0x70, 0x6c, 0x61, 0x63, 0x65, 0x49, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65,
	0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x37, 0x0a, 0x04,
	0x74, 0x79, 0x70, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x23, 0x2e, 0x63, 0x6f, 0x6d,
	0x2e, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x72, 0x6f, 0x75, 0x74, 0x65, 0x73, 0x2e, 0x74,
	0x72, 0x69, 0x70, 0x2e, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x54, 0x79, 0x70, 0x65, 0x52,
	0x04, 0x74, 0x79, 0x70, 0x65, 0x22, 0x35, 0x0a, 0x19, 0x53, 0x65, 0x74, 0x44, 0x65, 0x66, 0x61,
	0x75, 0x6c, 0x74, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2a, 0x21, 0x0a, 0x0b,
	0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x54, 0x79, 0x70, 0x65, 0x12, 0x08, 0x0a, 0x04, 0x68,
	0x6f, 0x6d, 0x65, 0x10, 0x00, 0x12, 0x08, 0x0a, 0x04, 0x77, 0x6f, 0x72, 0x6b, 0x10, 0x01, 0x32,
	0xff, 0x02, 0x0a, 0x14, 0x50, 0x61, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65, 0x72, 0x54, 0x72, 0x69,
	0x70, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x80, 0x01, 0x0a, 0x13, 0x41, 0x75, 0x74,
	0x6f, 0x43, 0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73,
	0x12, 0x32, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x72, 0x6f,
	0x75, 0x74, 0x65, 0x73, 0x2e, 0x74, 0x72, 0x69, 0x70, 0x2e, 0x41, 0x75, 0x74, 0x6f, 0x43, 0x6f,
	0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x33, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x6e, 0x65,
	0x63, 0x74, 0x72, 0x6f, 0x75, 0x74, 0x65, 0x73, 0x2e, 0x74, 0x72, 0x69, 0x70, 0x2e, 0x41, 0x75,
	0x74, 0x6f, 0x43, 0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x68, 0x0a, 0x0b, 0x54,
	0x72, 0x69, 0x70, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x12, 0x2a, 0x2e, 0x63, 0x6f, 0x6d,
	0x2e, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x72, 0x6f, 0x75, 0x74, 0x65, 0x73, 0x2e, 0x74,
	0x72, 0x69, 0x70, 0x2e, 0x54, 0x72, 0x69, 0x70, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x2b, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x63, 0x6f, 0x6e,
	0x6e, 0x65, 0x63, 0x74, 0x72, 0x6f, 0x75, 0x74, 0x65, 0x73, 0x2e, 0x74, 0x72, 0x69, 0x70, 0x2e,
	0x54, 0x72, 0x69, 0x70, 0x48, 0x69, 0x73, 0x74, 0x6f, 0x72, 0x79, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x7a, 0x0a, 0x11, 0x53, 0x65, 0x74, 0x44, 0x65, 0x66, 0x61,
	0x75, 0x6c, 0x74, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x12, 0x30, 0x2e, 0x63, 0x6f, 0x6d,
	0x2e, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x72, 0x6f, 0x75, 0x74, 0x65, 0x73, 0x2e, 0x74,
	0x72, 0x69, 0x70, 0x2e, 0x53, 0x65, 0x74, 0x44, 0x65, 0x66, 0x61, 0x75, 0x6c, 0x74, 0x41, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x31, 0x2e, 0x63,
	0x6f, 0x6d, 0x2e, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x72, 0x6f, 0x75, 0x74, 0x65, 0x73,
	0x2e, 0x74, 0x72, 0x69, 0x70, 0x2e, 0x53, 0x65, 0x74, 0x44, 0x65, 0x66, 0x61, 0x75, 0x6c, 0x74,
	0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22,
	0x00, 0x42, 0x07, 0x5a, 0x05, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x33,
}

var (
	file_trip_passenger_proto_rawDescOnce sync.Once
	file_trip_passenger_proto_rawDescData = file_trip_passenger_proto_rawDesc
)

func file_trip_passenger_proto_rawDescGZIP() []byte {
	file_trip_passenger_proto_rawDescOnce.Do(func() {
		file_trip_passenger_proto_rawDescData = protoimpl.X.CompressGZIP(file_trip_passenger_proto_rawDescData)
	})
	return file_trip_passenger_proto_rawDescData
}

var file_trip_passenger_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_trip_passenger_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_trip_passenger_proto_goTypes = []interface{}{
	(AddressType)(0),                    // 0: com.connectroutes.trip.AddressType
	(*TripHistoryRequest)(nil),          // 1: com.connectroutes.trip.TripHistoryRequest
	(*TripHistoryResponse)(nil),         // 2: com.connectroutes.trip.TripHistoryResponse
	(*SetDefaultAddressRequest)(nil),    // 3: com.connectroutes.trip.SetDefaultAddressRequest
	(*SetDefaultAddressResponse)(nil),   // 4: com.connectroutes.trip.SetDefaultAddressResponse
	(*Trip)(nil),                        // 5: com.connectroutes.trip.Trip
	(*AutoCompleteAddressRequest)(nil),  // 6: com.connectroutes.trip.AutoCompleteAddressRequest
	(*AutoCompleteAddressResponse)(nil), // 7: com.connectroutes.trip.AutoCompleteAddressResponse
}
var file_trip_passenger_proto_depIdxs = []int32{
	5, // 0: com.connectroutes.trip.TripHistoryResponse.trips:type_name -> com.connectroutes.trip.Trip
	0, // 1: com.connectroutes.trip.SetDefaultAddressRequest.type:type_name -> com.connectroutes.trip.AddressType
	6, // 2: com.connectroutes.trip.PassengerTripService.AutoCompleteAddress:input_type -> com.connectroutes.trip.AutoCompleteAddressRequest
	1, // 3: com.connectroutes.trip.PassengerTripService.TripHistory:input_type -> com.connectroutes.trip.TripHistoryRequest
	3, // 4: com.connectroutes.trip.PassengerTripService.SetDefaultAddress:input_type -> com.connectroutes.trip.SetDefaultAddressRequest
	7, // 5: com.connectroutes.trip.PassengerTripService.AutoCompleteAddress:output_type -> com.connectroutes.trip.AutoCompleteAddressResponse
	2, // 6: com.connectroutes.trip.PassengerTripService.TripHistory:output_type -> com.connectroutes.trip.TripHistoryResponse
	4, // 7: com.connectroutes.trip.PassengerTripService.SetDefaultAddress:output_type -> com.connectroutes.trip.SetDefaultAddressResponse
	5, // [5:8] is the sub-list for method output_type
	2, // [2:5] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_trip_passenger_proto_init() }
func file_trip_passenger_proto_init() {
	if File_trip_passenger_proto != nil {
		return
	}
	file_trip_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_trip_passenger_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TripHistoryRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_trip_passenger_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TripHistoryResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_trip_passenger_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetDefaultAddressRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_trip_passenger_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetDefaultAddressResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_trip_passenger_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_trip_passenger_proto_goTypes,
		DependencyIndexes: file_trip_passenger_proto_depIdxs,
		EnumInfos:         file_trip_passenger_proto_enumTypes,
		MessageInfos:      file_trip_passenger_proto_msgTypes,
	}.Build()
	File_trip_passenger_proto = out.File
	file_trip_passenger_proto_rawDesc = nil
	file_trip_passenger_proto_goTypes = nil
	file_trip_passenger_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// PassengerTripServiceClient is the client API for PassengerTripService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type PassengerTripServiceClient interface {
	AutoCompleteAddress(ctx context.Context, in *AutoCompleteAddressRequest, opts ...grpc.CallOption) (*AutoCompleteAddressResponse, error)
	TripHistory(ctx context.Context, in *TripHistoryRequest, opts ...grpc.CallOption) (*TripHistoryResponse, error)
	SetDefaultAddress(ctx context.Context, in *SetDefaultAddressRequest, opts ...grpc.CallOption) (*SetDefaultAddressResponse, error)
}

type passengerTripServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewPassengerTripServiceClient(cc grpc.ClientConnInterface) PassengerTripServiceClient {
	return &passengerTripServiceClient{cc}
}

func (c *passengerTripServiceClient) AutoCompleteAddress(ctx context.Context, in *AutoCompleteAddressRequest, opts ...grpc.CallOption) (*AutoCompleteAddressResponse, error) {
	out := new(AutoCompleteAddressResponse)
	err := c.cc.Invoke(ctx, "/com.connectroutes.trip.PassengerTripService/AutoCompleteAddress", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *passengerTripServiceClient) TripHistory(ctx context.Context, in *TripHistoryRequest, opts ...grpc.CallOption) (*TripHistoryResponse, error) {
	out := new(TripHistoryResponse)
	err := c.cc.Invoke(ctx, "/com.connectroutes.trip.PassengerTripService/TripHistory", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *passengerTripServiceClient) SetDefaultAddress(ctx context.Context, in *SetDefaultAddressRequest, opts ...grpc.CallOption) (*SetDefaultAddressResponse, error) {
	out := new(SetDefaultAddressResponse)
	err := c.cc.Invoke(ctx, "/com.connectroutes.trip.PassengerTripService/SetDefaultAddress", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PassengerTripServiceServer is the server API for PassengerTripService service.
type PassengerTripServiceServer interface {
	AutoCompleteAddress(context.Context, *AutoCompleteAddressRequest) (*AutoCompleteAddressResponse, error)
	TripHistory(context.Context, *TripHistoryRequest) (*TripHistoryResponse, error)
	SetDefaultAddress(context.Context, *SetDefaultAddressRequest) (*SetDefaultAddressResponse, error)
}

// UnimplementedPassengerTripServiceServer can be embedded to have forward compatible implementations.
type UnimplementedPassengerTripServiceServer struct {
}

func (*UnimplementedPassengerTripServiceServer) AutoCompleteAddress(context.Context, *AutoCompleteAddressRequest) (*AutoCompleteAddressResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AutoCompleteAddress not implemented")
}
func (*UnimplementedPassengerTripServiceServer) TripHistory(context.Context, *TripHistoryRequest) (*TripHistoryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method TripHistory not implemented")
}
func (*UnimplementedPassengerTripServiceServer) SetDefaultAddress(context.Context, *SetDefaultAddressRequest) (*SetDefaultAddressResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SetDefaultAddress not implemented")
}

func RegisterPassengerTripServiceServer(s *grpc.Server, srv PassengerTripServiceServer) {
	s.RegisterService(&_PassengerTripService_serviceDesc, srv)
}

func _PassengerTripService_AutoCompleteAddress_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AutoCompleteAddressRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PassengerTripServiceServer).AutoCompleteAddress(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/com.connectroutes.trip.PassengerTripService/AutoCompleteAddress",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PassengerTripServiceServer).AutoCompleteAddress(ctx, req.(*AutoCompleteAddressRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PassengerTripService_TripHistory_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TripHistoryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PassengerTripServiceServer).TripHistory(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/com.connectroutes.trip.PassengerTripService/TripHistory",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PassengerTripServiceServer).TripHistory(ctx, req.(*TripHistoryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PassengerTripService_SetDefaultAddress_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SetDefaultAddressRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PassengerTripServiceServer).SetDefaultAddress(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/com.connectroutes.trip.PassengerTripService/SetDefaultAddress",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PassengerTripServiceServer).SetDefaultAddress(ctx, req.(*SetDefaultAddressRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _PassengerTripService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "com.connectroutes.trip.PassengerTripService",
	HandlerType: (*PassengerTripServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "AutoCompleteAddress",
			Handler:    _PassengerTripService_AutoCompleteAddress_Handler,
		},
		{
			MethodName: "TripHistory",
			Handler:    _PassengerTripService_TripHistory_Handler,
		},
		{
			MethodName: "SetDefaultAddress",
			Handler:    _PassengerTripService_SetDefaultAddress_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "trip.passenger.proto",
}
