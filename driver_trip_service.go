package main

import (
	"context"
	"gitlab.com/connectroutes/routes-go-lib/models"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"routes-trip-service/proto"
)

type DriverService struct {
}

func (d DriverService) SetLocation(ctx context.Context, request *proto.SetLocationRequest) (*proto.SetLocationResponse, error) {
	driver, _ := getDriver(ctx)

	/* if !driver.Online {
		return nil, status.Errorf(codes.InvalidArgument,
			"Cannot update location when offline")
	}*/

	// TODO server side rate limiting
	err := models.UpdateDriver(ctx, db, driver.Id, map[string]interface{}{
		"location": map[string]interface{}{
			"type":        "Point",
			"coordinates": []float64{request.Location.Lon, request.Location.Lat},
		},
	})

	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"Cannot update driver location")
	}

	return &proto.SetLocationResponse{Message: "Ok"}, nil

}

func (d DriverService) AutoCompleteAddress(ctx context.Context, request *proto.AutoCompleteAddressRequest) (*proto.AutoCompleteAddressResponse, error) {
	return AutoCompleteAddress(ctx, request)
}

func (d DriverService) SetDriverAvailability(ctx context.Context, request *proto.SetDriverAvailabilityRequest) (*proto.SetDriverAvailabilityResponse, error) {
	driver, _ := getDriver(ctx)

	err := models.UpdateDriver(ctx, db, driver.Id, map[string]interface{}{
		"online": request.Online,
	})

	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"Cannot update driver status")
	}

	return &proto.SetDriverAvailabilityResponse{Message: "Ok"}, nil
}

func getDriver(ctx context.Context) (*models.Driver, error) {
	user, err := models.GetUserFromContext(ctx)

	if err != nil {
		return nil, err
	}
	return &user.Driver, err
}
